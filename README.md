# NEN HACKATHON 
This is the homepage of our product for the NEN Hackathon! Welcome! 
## TEAM MEMBERS 
- Wouter Lubbers , Semmtech
- Louise Dam, Semmtech
- Coen van Gruijthuijsen, Semmtech
- Bart Schrijnen, StarTXT
- Cathérine Prissens, Neanex

## Outline of this repository 
- In `data`, You can find our used data files in a turtle-format. We have only included data that are made publicly by the NEN itself. 
- In `code`, You can find the script we used to transfer XML to CSV. We have also supplied this code as a pull-request to the [NEN repository](https://github.com/Netherlands-Standardization-Institute/xml2csv). 

## Deliverables overview 
The SPARQL Endpoints are protected with a password. You can contact Wouter Lubbers (wouterlubbers@semmtech.nl) to get access. 
Deliverable | Information Type |  LINK 
--- | --- | --- 
Poster | Link | https://gitlab.com/semmtech-company/public/NEN_Hackathon/-/tree/main/Presentation/Poster.pdf
Slides | Link | https://gitlab.com/semmtech-company/public/NEN_Hackathon/-/tree/main/Presentation/Slides.pdf
XML2CSV | Code | https://gitlab.com/semmtech-company/public/NEN_Hackathon/-/tree/main/code 
NEN- Catalog | Link |  https://hub.laces.tech/nen/hackathon/catalogus/sparql
NEN- Thesaurus | Link |  https://hub.laces.tech/nen/hackathon/vocabulary/sparql 
NEN-standards Requirements Extraction | Link | https://hub.laces.tech/nen/hackathon/eisen-set/sparql 
NEN- Thesaurus to Requirements | Link | https://hub.laces.tech/nen/hackathon/object-eis-link/sparql
NEN- Thesaurus to NL-SfB | Link | https://hub.laces.tech/nen/hackathon/nl-sfb/versions/4/sparql 
End use: Laces Linked Data Viewer + Neanex 3D Viewer | Demo | Live at the Jury Table at 16:00 CEST ! 
